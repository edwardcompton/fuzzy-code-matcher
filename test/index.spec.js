'use strict';

const assert = require('assert');

const fuzzyKeyMatcher = require('../index');

describe('fuzzyKeyMatcher', () => {
  let tagPreferences, elements, query;

  beforeEach(() => {
    tagPreferences = ['a', 'b', 'c', 'd'];

    elements = [
      {
        tags: {},
        value: 0
      }, 
      {
        tags: { a:1 },
        value: 1
      },
      {
        tags: { b:2 },
        value: 2
      },
      {
        tags: { a:1, b:3 },
        value: 3
      },
      {
        tags: { b:2, c:1 },
        value: 4
      }
  ];
  });

  it('test 1', () => {
    query = {a:1, b:3, c:2};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 3);
  });

  it('test 2', () => {
    query = {a:2, b:2};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 2);
  });

  it('test 3', () => {
    query = {a:1, b:2};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 2);
  });

  it('test 4', () => {
    query = {a:1, c:2};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 1);
  });

  it('test 5', () => {
    query = {b:2, c:1};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 4);
  });

  it('test 6', () => {
    query = {a:1, b:2, c:1};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 4);
  });

  it('test 7', () => {
    query = {a:2, c:3, b:6};
    let result = fuzzyKeyMatcher(tagPreferences, elements, query);

    assert.strictEqual(result, 0);
  });
});
